package main

import (
	"fmt"
	"testing"

	"github.com/tommasomoretto/lesson8/client/client"
	"github.com/tommasomoretto/lesson8/client/client/products"
)

func TestOurClient(t *testing.T) {
	cfg := client.DefaultTransportConfig().WithHost("localhost:9090")
	c := client.NewHTTPClientWithConfig(nil, cfg)
	prods, err := c.Products.ListProducts(products.NewListProductsParams())

	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(prods)
}
